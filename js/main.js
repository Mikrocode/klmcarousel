/* settings */
var pages,
    prev = document.getElementById('klm_prevArrow_white'),
    next = document.getElementById('klm_nextArrow_white'),
    currentPage = 0, // starting slide
    lastPage,
    photoContainer = document.querySelector ('.klm_destinationsCarousel_photos ul') ;

var showSlide = function (lastslide, currentSlide, totalSlides){
    lastPage = lastslide;
    lastPage.classList.remove ('active');
    pages[currentSlide].classList.add ('active');
    currentPage = currentSlide;
    photoContainer.style.left = (-480*currentPage)+"px";
}

var mobileMenu = function(){
    
if (document.body.clientWidth>=768){
    pages = document.querySelectorAll('#klm_destinationsCarousel_indicator .klm_destinationsCarousel_page');
    document.getElementById ('klm_destinationsCarousel_mobileindicator').style.display ='none';
    document.getElementById ('klm_destinationsCarousel_indicator').style.display ='block';

}else{
    pages = document.querySelectorAll('#klm_destinationsCarousel_mobileindicator .klm_destinationsCarousel_page');
    document.getElementById ('klm_destinationsCarousel_indicator').style.display ='none';
    document.getElementById ('klm_destinationsCarousel_mobileindicator').style.display ='block';


}
Array.prototype.forEach.call(pages,function(page,i){   
    pages[i].classList.remove ('active');

    page.addEventListener('click', function(){
        showSlide (pages[currentPage], i, pages.length);
}, false);

  }) ;
pages[currentPage].classList.add ('active');
}


/* init */
mobileMenu();
pages[currentPage].classList.add ('active');

document.body.onresize = function() {
    mobileMenu(document.body.clientWidth);
};





next.addEventListener("click", function(){
    var lastslide = currentPage;
    console.info ('last',currentPage)
     if (lastslide<pages.length-1){
        currentPage++

        console.log ('+',currentPage,pages.length)
    }else{
        currentPage = 0;

    }
    console.log (currentPage)
    showSlide (pages[lastslide], currentPage, pages.length);
});

prev.addEventListener("click", function(){
    var lastslide = currentPage;
     if (lastslide>0){
        currentPage--
    }else{
        currentPage = pages.length-1;

    }
    console.error (currentPage);
    showSlide (pages[lastslide], currentPage, pages.length);
});



